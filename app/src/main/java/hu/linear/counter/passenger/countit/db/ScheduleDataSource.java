package hu.linear.counter.passenger.countit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Adatbazis muveletek a menetrendi adatokon
 *
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class ScheduleDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MyDBHelper dbHelper;
    private String[] allColumns = {
            ScheduleTable.COLUMN_ID,
            ScheduleTable.COLUMN_TITLE,
            ScheduleTable.COLUMN_DIRECTION,
            ScheduleTable.COLUMN_START,
            ScheduleTable.COLUMN_DESTINATION,
            ScheduleTable.COLUMN_POS,
            ScheduleTable.COLUMN_STATION
    };

    public ScheduleDataSource(Context context) {
        dbHelper = new MyDBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {

        dbHelper.close();

    }

    /**
     * Egy viszonylat letrehozasa az adatbazisban
     * @param schedule  Schedule objektum egy peldanya
     */
    public void createSchedule(Schedule schedule) {
        ContentValues values = new ContentValues();

        values.put(ScheduleTable.COLUMN_TITLE, schedule.getTitle());
        values.put(ScheduleTable.COLUMN_DIRECTION, schedule.getDirection());
        values.put(ScheduleTable.COLUMN_START, schedule.getStart());
        values.put(ScheduleTable.COLUMN_DESTINATION, schedule.getDestinations());
        values.put(ScheduleTable.COLUMN_POS, schedule.getPosition());
        values.put(ScheduleTable.COLUMN_STATION, schedule.getStation());

        long insertId = database.insert(ScheduleTable.TABLE_NAME, null, values);
    }

    /**
     * Viszonylat modositasa, ha nincs id akkor ujkent lesz beillesztve
     * egyebkent meg felulirva a meglevo
     * @param schedule Schedule objektum egy peldanya
     */
    public void updateSchedule(Schedule schedule) {
        ContentValues values = new ContentValues();

        values.put(ScheduleTable.COLUMN_ID, schedule.getId());
        values.put(ScheduleTable.COLUMN_TITLE, schedule.getTitle());
        values.put(ScheduleTable.COLUMN_DIRECTION, schedule.getDirection());
        values.put(ScheduleTable.COLUMN_START, schedule.getStart());
        values.put(ScheduleTable.COLUMN_DESTINATION, schedule.getDestinations());
        values.put(ScheduleTable.COLUMN_POS, schedule.getPosition());
        values.put(ScheduleTable.COLUMN_STATION, schedule.getStation());

        if (schedule.getId() == 0) {

            database.insert(ScheduleTable.TABLE_NAME, null, values);

        } else {

            database.replace(ScheduleTable.TABLE_NAME, null, values);

        }
    }

    /**
     * List-ben szereplo menetrend adatok feltoltese
     * Gyors feltoltes erdekeben tranzakcioval es prepare statement
     * alkalmazasaval
     * @param schedules Schedule objektumok listaja
     */
    public void bulkInsertSchedule(List<Schedule> schedules) {

        String sql = "INSERT INTO " + ScheduleTable.TABLE_NAME + " ("
                + ScheduleTable.COLUMN_TITLE + ", "
                + ScheduleTable.COLUMN_DIRECTION + ", "
                + ScheduleTable.COLUMN_START + ", "
                + ScheduleTable.COLUMN_DESTINATION + ", "
                + ScheduleTable.COLUMN_POS + ", "
                + ScheduleTable.COLUMN_STATION
                + ") values (?, ?, ?, ?, ?, ?);";

        database.beginTransaction();
        SQLiteStatement stmt = database.compileStatement(sql);

        for (Schedule schedule : schedules) {

            stmt.bindString(1, schedule.getTitle());
            stmt.bindString(2, schedule.getDirection());
            stmt.bindString(3, schedule.getStart());
            stmt.bindString(4, schedule.getDestinations());
            stmt.bindLong(5, schedule.getPosition());
            stmt.bindString(6, schedule.getStation());

            long entryID = stmt.executeInsert();
            stmt.clearBindings();

        }

        database.setTransactionSuccessful();
        database.endTransaction();

    }

    /**
     * Egy viszonylat torlese id alapjan
     * @param schedule Schedule objektum egy peldanya
     */
    public void deleteSchedule(Schedule schedule) {
        long id = schedule.getId();
        database.delete(ScheduleTable.TABLE_NAME, ScheduleTable.COLUMN_ID
                + " = " + id, null);
    }

    /**
     * Osszes viszonylat torlese
     * Adatok torlese hosszu idott vesz igenybe,
     * ezert drop table, majd ujra letrehozva
     */
    public void deleteAllSchedule() {
        //database.delete(ScheduleTable.TABLE_NAME, null, null);
        database.execSQL("DROP TABLE " + ScheduleTable.TABLE_NAME);
        ScheduleTable.onCreate(database);
    }

    /**
     * Kurzor atalakitasa Schedule objektumma
     * @param cursor Cursor object
     * @return Schedule
     */
    private Schedule cursorToSchedule(Cursor cursor) {

        return new Schedule(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5),
                cursor.getString(6));
    }


    /**
     * Osszes viszonylat listaja
     * @return List<Schedule>
     */
    public List<Schedule> getAllSchedules() {
        List<Schedule> schedules = new ArrayList<>();

        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Schedule schedule = cursorToSchedule(cursor);
            schedules.add(schedule);
            cursor.moveToNext();
        }

        cursor.close();
        return schedules;
    }

    /**
     * Viszonylatok listaja
     * @return Viszonylatok listaja ArrayList-ben
     */
    public List<Schedule> getScheduleTitles() {
        List<Schedule> schedules = new ArrayList<>();

        String groupBy = ScheduleTable.COLUMN_TITLE;
        String orderBy = ScheduleTable.COLUMN_TITLE + " ASC";

        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, null, null, groupBy, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Schedule schedule = cursorToSchedule(cursor);
            schedules.add(schedule);
            cursor.moveToNext();
        }

        cursor.close();
        return schedules;
    }

    /**
     * Egy viszonylatok nev alapjan
     * @return Viszonylatok listaja ArrayList-ben
     */
    public Schedule getScheduleByTitle(String title) {
        Schedule schedules;

        String where = ScheduleTable.COLUMN_TITLE + "='" + title + "'";
        String groupBy = ScheduleTable.COLUMN_TITLE;
        String orderBy = ScheduleTable.COLUMN_TITLE + " ASC";

        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, where, null, groupBy, null, orderBy);

        cursor.moveToFirst();

        Schedule schedule = cursorToSchedule(cursor);


        cursor.close();
        return schedule;
    }

    /**
     * Iranyok listaja viszonylat alapjan
     * @param title Schedule objektum ami alapjan az iranyokat megkapjuk
     * @return List<Schedule> iranyok
     */
    public List<Schedule> getScheduleDirections(Schedule title) {
        List<Schedule> schedules = new ArrayList<>();

        String where = ScheduleTable.COLUMN_TITLE + "='" + title.getTitle() + "'";
        String groupBy = ScheduleTable.COLUMN_DESTINATION;
        String orderBy = ScheduleTable.COLUMN_DIRECTION + " ASC";

        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, where, null, groupBy, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Schedule schedule = cursorToSchedule(cursor);
            schedules.add(schedule);
            cursor.moveToNext();
        }

        cursor.close();
        return schedules;
    }

    /**
     * Adott iranyhoz tartozo indulasi idok, az aktualis idohoz kepest
     * a +/-1 oran belul indulo jaratok
     * @param direction Schedule objektum, ami az adott irany adatait tartalmazza
     * @return List<Schedule>
     */
    public List<Schedule> getScheduleTimes(Schedule direction) {
        List<Schedule> schedules = new ArrayList<>();

        String current_time = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());

        String where = ScheduleTable.COLUMN_TITLE + "='" + direction.getTitle() + "'";
        where += " AND " + ScheduleTable.COLUMN_DESTINATION + "='" + direction.getDestinations() + "'";

        if (current_time.substring(0,2).equals("23")) {
            where += " AND " + ScheduleTable.COLUMN_START + " BETWEEN strftime('%H:%M','" + current_time + "','-1 hour') AND  '23:59'";
        } else if (current_time.substring(0,2).equals("00")) {
            where += " AND " + ScheduleTable.COLUMN_START + " BETWEEN '00:00' AND  strftime('%H:%M','" + current_time + "','+1 hour')";
        } else{
            where += " AND " + ScheduleTable.COLUMN_START + " BETWEEN strftime('%H:%M','" + current_time + "','-1 hour') AND  strftime('%H:%M','" + current_time + "','+1 hour')";
        }

        String groupBy = ScheduleTable.COLUMN_START;
        String orderBy = ScheduleTable.COLUMN_START + " ASC";
        /*
        Log.d("ScheduleDataSource", "getScheduleTimes where " + where);
        Log.d("ScheduleDataSource", "getScheduleTimes group " + groupBy);
        Log.d("ScheduleDataSource", "getScheduleTimes order " + orderBy);
        */
        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, where, null, groupBy, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Schedule schedule = cursorToSchedule(cursor);
            schedules.add(schedule);
            cursor.moveToNext();
        }

        cursor.close();
        return schedules;
    }

    /**
     * Megallok listaja
     * @param title Jarat szama
     * @param destinations irany
     * @param time indulas
     * @return List<Schedule>
     */
    public List<Schedule> getStations(String title, String destinations, String time) {
        List<Schedule> schedules = new ArrayList<>();

        String where = ScheduleTable.COLUMN_TITLE + "='" + title + "'";
        where += " AND " + ScheduleTable.COLUMN_DESTINATION + "='" + destinations + "'";
        where += " AND " + ScheduleTable.COLUMN_START + "='" + time +"'";
        String orderBy = ScheduleTable.COLUMN_POS + " ASC;";

        Log.d("ScheduleDataSource", where);
        Log.d("ScheduleDataSource", orderBy);

        Cursor cursor = database.query(ScheduleTable.TABLE_NAME,
                allColumns, where, null, null, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Schedule schedule = cursorToSchedule(cursor);
            schedules.add(schedule);
            cursor.moveToNext();
        }

        cursor.close();
        return schedules;
    }
}
