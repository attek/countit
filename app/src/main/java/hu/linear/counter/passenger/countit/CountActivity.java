package hu.linear.counter.passenger.countit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import hu.linear.counter.passenger.countit.adapter.CountPagerAdapter;
import hu.linear.counter.passenger.countit.db.Schedule;
import hu.linear.counter.passenger.countit.db.ScheduleDataSource;
import hu.linear.counter.passenger.countit.util.CountDataToFile;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;
import hu.linear.counter.passenger.countit.view.HCViewPager;

/**
 * Szamlalas kepernyo
 * Created on 2015.09.15.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class CountActivity extends AppCompatActivity implements CountPagerAdapter.ButtonClick, PopupFragment.IDialogDataPass {

    private ScheduleDataSource dataSource;
    private ArrayList<Schedule> stations;
    public static final String TAG = "CountActivity";
    private HCViewPager vpCount;
    private CountPagerAdapter aPageAdapter;
    private int pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.count);

        MyApplication.setCustomActionbar(this, R.drawable.icon_main, R.string.title_count_activity, false);

        Button btnCancel;
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setTransformationMethod(null);

        TextView tvVehicle;
        tvVehicle = (TextView) findViewById(R.id.tvVehicle);
        TextView tvDestination;
        tvDestination = (TextView) findViewById(R.id.tvDestination);

        vpCount = (HCViewPager) findViewById(R.id.vpCount);
        vpCount.setPagingEnabled(false);

        tvVehicle.setText(MyApplication.CHOSEN_VEHICLE);
        tvDestination.setText(MyApplication.CHOSEN_DIRECTION);

        dataSource = new ScheduleDataSource(CountActivity.this);
        dataSource.open();

        stations = new ArrayList<>();
        stations = (ArrayList<Schedule>) dataSource.getStations(MyApplication.CHOSEN_VEHICLE,
                MyApplication.CHOSEN_DIRECTION, MyApplication.CHOSEN_TIME);

        aPageAdapter = new CountPagerAdapter(this, stations, this);
        vpCount.setAdapter(aPageAdapter);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                PopupFragment popup = new PopupFragment();
                popup.setListener(CountActivity.this);
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.are_you_sure_to_interrupt));
                b.putString("ok", getString(R.string.dialogOk));
                b.putString("cancel", getString(R.string.dialogCancel));
                b.putBoolean("fromCancelButton" , true);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataSource.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    @Override
    public void onNextClick() {

        int currentPage = vpCount.getCurrentItem();
        int totalPages = vpCount.getAdapter().getCount();

        Schedule currentStation = ((CountPagerAdapter) vpCount.getAdapter()).getItem(currentPage);

        int nextPage = currentPage+1;

        if (currentStation.getUp() == 0 && currentStation.getDown() == 0 && currentPage != 0) {

            PopupFragment popup = new PopupFragment();
            popup.setListener(CountActivity.this);
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.up_down_take));
            b.putString("ok", getString(R.string.dialogOk));
            b.putString("cancel", getString(R.string.dialogCancel));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

            pageNumber = nextPage;

        } else if (currentStation.getUp() == 0 && currentStation.getDown() == 0 && currentPage == 0) {

            PopupFragment popup = new PopupFragment();
            popup.setListener(CountActivity.this);
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.up_take));
            b.putString("ok", getString(R.string.dialogOk));
            b.putString("cancel", getString(R.string.dialogCancel));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

            pageNumber = nextPage;

        } else {

            if (nextPage <= totalPages) vpCount.setCurrentItem(nextPage, true);

        }

    }

    @Override
    public void onPrevClick() {

        int currentPage = vpCount.getCurrentItem();
        //int totalPages = vpCount.getAdapter().getCount();


        int previousPage = currentPage-1;

        if (previousPage >= 0) vpCount.setCurrentItem(previousPage, true);

    }

    @Override
    public void onEndClick() {

        int currentPage = vpCount.getCurrentItem();
        //int totalPages = vpCount.getAdapter().getCount();

        Schedule currentStation = ((CountPagerAdapter) vpCount.getAdapter()).getItem(currentPage);

        if (currentStation.getDown() == 0) {

            PopupFragment popup = new PopupFragment();
            popup.setListener(CountActivity.this);
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.down_take));
            b.putString("ok", getString(R.string.dialogOk));
            b.putString("cancel", getString(R.string.dialogCancel));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

            pageNumber = currentPage + 1;

        } else {

            endCounting();

        }

    }

    //Volt fel- és leszálló a megállóban?” Igenre dobjon vissza  a képernyőre, nemre dobjon a következő képernyőre
    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {
        if (fromCancelButton) {
            finish();
            Intent i = new Intent();
            i.setClass(CountActivity.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

        if (!fromCancelButton) {
            int totalPages = vpCount.getAdapter().getCount();

            if (pageNumber != totalPages) {
                vpCount.setCurrentItem(pageNumber, true);
            } else {

                endCounting();

            }
        }

    }

    private void endCounting() {

        //File név képzése: user_jarműazonosító_viszonylat_irány_indulásiidő_dátum_filekészítésidő.zip
        String time_stamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        String username = MyApplication.convertToFileName(MyApplication.sp.getString(MyApplication.USER_NAME, ""));

        String filename = username + "_" +
                MyApplication.CHOSEN_CODE + "_" +
                aPageAdapter.getItem(0).getTitle() + "_" +
                aPageAdapter.getItem(0).getDirection()  + "_" +
                aPageAdapter.getItem(0).getStart().replace(":", "") + "_" +
                time_stamp + ".txt";

        CountDataToFile dataToFile = new CountDataToFile(this, filename);

        for (int index = 0; index <= aPageAdapter.getCount()-1; index++) {

            dataToFile.appendData(aPageAdapter.getItem(index).getOutput());
        }

        dataToFile.compressFile();

        finish();
        Intent i = new Intent();
        i.setClass(CountActivity.this, CountEnd.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}

