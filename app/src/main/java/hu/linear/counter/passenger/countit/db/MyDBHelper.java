package hu.linear.counter.passenger.countit.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Adatbazis helper
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class MyDBHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "hclinear.db";
	private static final int DATABASE_VERSION = 6;
	
	public MyDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		//Minden egyes tabla letrehozasa
		ScheduleTable.onCreate(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		//Minden egyes tabla upgrade-je
		ScheduleTable.onUpgrade(database, oldVersion, newVersion);
	}


}
