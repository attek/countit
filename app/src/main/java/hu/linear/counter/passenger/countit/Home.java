package hu.linear.counter.passenger.countit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import hu.linear.counter.passenger.countit.util.CountDataToFile;
import hu.linear.counter.passenger.countit.util.LogToFile;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;
import hu.linear.counter.passenger.countit.util.ProcessSchedule;

/**
 * Home screen
 *
 * Sorrend
 * MainActivity
 * 0.Home
 * 1.Choose
 * 2.DoorsCount
 * 3.ChooseDoor
 * 4.Start
 * 5.CountActivity
 *
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.4
 */
public class Home extends AppCompatActivity {

    private Button btnCount;
    private Button btnSync;
    private Boolean doubleBackToExitPressedOnce = false;

    private String currentDate;
    private ProgressDialog dialog;
    private ProgressDialog dialog3;

    public static final String TAG = "HomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        btnCount = (Button) findViewById(R.id.btnCount);
        btnSync = (Button) findViewById(R.id.btnSync);
        Button btnLogout = (Button) findViewById(R.id.btnLogout);
        Button btnInfo = (Button) findViewById(R.id.btnInfo);

        MyApplication.setCustomActionbar(this, R.drawable.icon_main, R.string.app_name, false);

        btnSync.setEnabled(false);

        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());

        //Menetrend letoltese ha az adott napon meg nem lett letoltve
        if (!MyApplication.sp.getString(MyApplication.SCHEDULE_DOWNLOAD_DATE, "").equals(currentDate)) {

            getScheduleUrl();

        }

        /**
         * Szamolas
         */
        btnCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(Home.this, Choose.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        /**
         * Informacios kepernyo
         */
        btnInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent();

                i.setClass(Home.this, Info.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        btnSync.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (MyApplication.isOnline(Home.this)) {

                    String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
                    String modifiedDate;

                    CountDataToFile countDataToFile = new CountDataToFile(Home.this);

                    File uploadDir = new File(countDataToFile.getFolder());
                    File[] fList = uploadDir.listFiles();
                    for (File file : fList) {

                        if (file.isFile()) {

                            modifiedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(file.lastModified());
                            if (currentDate.equals(modifiedDate)) {

                                if (MyApplication.enableLog) Log.d(TAG, "uploadFile: " + file.getName());

                                uploadFile(file.getName(), file);

                            }

                        }
                    }
                } else {

                    PopupFragment popup = new PopupFragment();
                    Bundle b = new Bundle();
                    FragmentManager fm = getSupportFragmentManager();
                    b.putString("text", getString(R.string.connection_error));
                    popup.setArguments(b);
                    popup.show(fm, PopupFragment.TAG);

                }

            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (isFileForUpload()) {

                    PopupFragment popup = new PopupFragment();
                    Bundle b = new Bundle();
                    FragmentManager fm = getSupportFragmentManager();
                    b.putString("text", getString(R.string.there_is_file_to_upload));
                    popup.setArguments(b);
                    popup.show(fm, PopupFragment.TAG);

                } else{

                    logOut(true);

                }

            }
        });

    }

    private void logOut(boolean popupWindow) {
        /*
        SharedPreferences.Editor et = MyApplication.sp.edit();
        et.remove(MyApplication.USER_NAME);
        et.remove(MyApplication.PASSWORD);
        et.apply();
        */

        if (popupWindow) {
            PopupFragment popup = new PopupFragment();
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("logout", "logout");
            b.putString("text", getString(R.string.thank_you));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                    /*
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_MAIN);
                    i.addCategory(Intent.CATEGORY_HOME);
                    Home.this.startActivity(i);
                    */
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("EXIT", true);
                    startActivity(i);
                }
            }, 2000);
        } else {

            finish();
            /*
            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            Home.this.startActivity(i);
            */

            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("EXIT", true);
            startActivity(i);

        }


    }

    /**
     * Adott napi menetrend zip letoltese
     * @param url Loetoltendo file url-je
     */
    private void DownloadSchedule(String url) {

        dialog3 = ProgressDialog.show(Home.this, getString(R.string.download_schedule), getString(R.string.please_wait));

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(Home.this) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                if (dialog3.isShowing()) {
                    try {
                        dialog3.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                PopupFragment popup = new PopupFragment();
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.schedule_dl_error) + statusCode);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {

                //file letoltesekor az adott napi datumot lementjuk a sharedprefernces-be
                SharedPreferences.Editor et = MyApplication.sp.edit();
                et.putString(MyApplication.SCHEDULE_DOWNLOAD_DATE, currentDate);
                et.apply();


                String path = new LogToFile(Home.this).getFolder() + "/" + MyApplication.SCHEDULE_FILE_NAME;
                File file = new File(path);
                if (file.delete()) System.out.println("schedule file deleted");

                if (MyApplication.enableLog) Log.d(TAG, "letoltott file path: " + response.getAbsoluteFile());

                try {
                    MyApplication.copy(response, file);

                    //Menetrend adatbazisba toltese a hatterben
                    new ProcessSchedule(Home.this, true).execute();

                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("can not copy schedule.zip");
                }

                if (dialog3.isShowing()) {
                    try {
                        dialog3.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * Adatbazis utvonalanak lekerese,
     * ha megkapjuk akkor DownloadSchedule-el letoltjuk
     */
    private void getScheduleUrl() {

        if (MyApplication.isOnline(Home.this)) {

            dialog = ProgressDialog.show(Home.this, getString(R.string.request_schedule_url), getString(R.string.please_wait));

            RequestParams params = new RequestParams();
            params.put("ADATBAZIS", "");
            params.put("nev", MyApplication.sp.getString(MyApplication.USER_NAME, ""));
            params.put("jelszo", MyApplication.sp.getString(MyApplication.PASSWORD, ""));

            if (MyApplication.enableLog) Log.d(TAG, "params: " + params);

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(MyApplication.API_URL, params, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    // called before request is started
                    Log.d(TAG, "onStart");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                    if (MyApplication.enableLog) Log.d(TAG, new String(response));
                    try {

                        JSONObject jsonObj = new JSONObject(new String(response));

                        JSONObject HCObject = jsonObj.getJSONObject("utasszamlalas");


                        if (MyApplication.enableLog)
                            Log.d(TAG, "utvonal: " + HCObject.getString("utvonal"));


                        if (HCObject.getString("utvonal").equals("nincs")) {

                            //’nincs’ a válasz, ha jogosult a felhasználó az aznapi adatbázis letöltésére a jelszó alapján, de
                            //nincs a db könyvtárban a szükséges zip file.

                            PopupFragment popup = new PopupFragment();
                            Bundle b = new Bundle();
                            FragmentManager fm = getSupportFragmentManager();
                            b.putString("text", getString(R.string.schedule_does_not_exists));
                            popup.setArguments(b);
                            popup.show(fm, PopupFragment.TAG);
                            btnCount.setEnabled(false);

                        } else if (HCObject.getString("utvonal").length() > 5) {

                            DownloadSchedule(HCObject.getString("utvonal"));

                        } else if (HCObject.getString("utvonal").equals("")) {
                            //üres a válasz – ha nem jogosult, vagy nem volt aznap érvényes bejelentkezése.
                            /*
                            PopupFragment popup = new PopupFragment();
                            Bundle b = new Bundle();
                            FragmentManager fm = getSupportFragmentManager();
                            b.putString("text", getString(R.string.not_valid_schedule_req));
                            popup.setArguments(b);
                            popup.show(fm, PopupFragment.TAG);
                            */
                            logOut(false);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // Remove loading
                    if (dialog.isShowing()) {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable error) {

                    if (dialog.isShowing()) {
                        try {
                            dialog.dismiss();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    PopupFragment popup = new PopupFragment();
                    Bundle b = new Bundle();
                    FragmentManager fm = getSupportFragmentManager();
                    b.putString("text", getString(R.string.error_file_upload) + statusCode);
                    popup.setArguments(b);
                    popup.show(fm, PopupFragment.TAG);

                }
            });

        } else {

            PopupFragment popup = new PopupFragment();
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.connection_error));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);
        }

    }

    /**
     * Toast megjelenitese
     * @param msg Message
     */
    void showToast(CharSequence msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {

            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            this.startActivity(i);
        }
        this.doubleBackToExitPressedOnce = true;
        showToast(getString(R.string.sendtoback));
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);

    }

    @Override
    public void onStart() {
        super.onStart();

        if (isFileForUpload()) btnSync.setEnabled(true);
        else btnSync.setEnabled(false);

        //Mentett adatok torlese, ha Vissza a fomenube gombrol jott, akkor ne jegyezzuk
        // meg a jarat szamat es a viszonylatot
        MyApplication.CHOSEN_VEHICLE = "";
        MyApplication.CHOSEN_DIRECTION = "";
        MyApplication.CHOSEN_TIME = "";
        MyApplication.CHOSEN_CODE = "";
        MyApplication.CHOSEN_DOORS = "0";
        MyApplication.VEHICLE_DOORS = 0;

    }

    /**
     * Van-e feltoltendo file a mai napra?
     * @return boolean
     */
    private boolean isFileForUpload() {

        boolean isFileForUpload = false;
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
        String modifiedDate;

        CountDataToFile countDataToFile = new CountDataToFile(this);

        File uploadDir = new File(countDataToFile.getFolder());
        File[] fList = uploadDir.listFiles();
        for (File file : fList) {

            if (file.isFile()) {

                modifiedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(file.lastModified());
                if (currentDate.equals(modifiedDate)) isFileForUpload = true;

            }
        }

        return isFileForUpload;

    }

    /**
     * Ha a feltoltes rendben akkor ezt logoljuk, valamit a file-t toroljuk
     * @param filename Feltoltesre kerule file neve
     * @param file Foltoltesre szant file
     */
    private void uploadFile(final String filename, final File file) {

        final ProgressDialog dialogUpload = ProgressDialog.show(Home.this, "File feltöltés!", filename);

        RequestParams params = new RequestParams();
        params.put("FELTOLT", "");
        params.put("nev",  MyApplication.sp.getString(MyApplication.USER_NAME, ""));
        params.put("jelszo", MyApplication.sp.getString(MyApplication.PASSWORD, ""));

        try {
            params.put("ujcsv", file);
        } catch(FileNotFoundException e) {
            Log.d(TAG, "ujcsv FileNotFoundException");
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(MyApplication.API_URL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                Log.d(TAG, "onStart");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG, "onSuccess: " + statusCode + " response: " + new String(response));
                String apiResponse = new String(response);

                try {
                    JSONObject jsonObj = new JSONObject(apiResponse);

                    JSONObject HCObject = jsonObj.getJSONObject("utasszamlalas");

                    if  (HCObject.getString("eredmeny").equals("rendben")) {

                        LogToFile logToFile = new LogToFile(Home.this);
                        logToFile.appendLog(filename + " feltoltve");
                        logToFile.appendLog(HCObject.getString("statusz"));

                        //File torlese, es vizsgalat, hogy van-e meg file
                        if (file.delete()) {

                            System.out.println(filename + " file deleted");
                            logToFile.appendLog(filename + " torolve");

                            if (isFileForUpload()) btnSync.setEnabled(true);
                            else btnSync.setEnabled(false);
                        }

                    } else if (HCObject.getString("eredmeny").equals("hiba")) {

                        PopupFragment popup = new PopupFragment();
                        Bundle b = new Bundle();
                        FragmentManager fm = getSupportFragmentManager();
                        b.putString("text", HCObject.getString("statusz"));
                        popup.setArguments(b);
                        popup.show(fm, PopupFragment.TAG);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (dialogUpload.isShowing()) {
                    try {
                        dialogUpload.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                if (dialogUpload.isShowing()) {
                    try {
                        dialogUpload.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                PopupFragment popup = new PopupFragment();
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.error_file_upload) + statusCode);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

}
