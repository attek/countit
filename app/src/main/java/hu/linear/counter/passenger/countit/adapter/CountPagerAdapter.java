package hu.linear.counter.passenger.countit.adapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.db.Schedule;

/**
 * Pager a szamlalas viewpager-hez
 * Created on 2015.09.15.
 *
 * @author Attila Pest
 * @version 1.1
 */

public class CountPagerAdapter extends PagerAdapter {
    private final ArrayList<Schedule> rows;
    private Context mContext;
    private static final String TAG = "CountPagerAdapter";
    private ButtonClick buttonClick;

    public CountPagerAdapter(final Context context, final ArrayList<Schedule> stations, ButtonClick aListener) {
        mContext = context;
        rows = stations;
        buttonClick = aListener;
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    public Schedule getItem(int position) {
        return rows.get(position);
    }

    public void add(Schedule aStation) {
        rows.add(aStation);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Context context = mContext;
        final Schedule row = rows.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView;
        itemView = inflater.inflate(R.layout.count_pager_row, null);


        TextView tvStation = (TextView) itemView.findViewById(R.id.tvStation);
        Button btnPrev = (Button) itemView.findViewById(R.id.btnPrev);
        Button btnNext = (Button) itemView.findViewById(R.id.btnNext);
        final EditText etUp = (EditText) itemView.findViewById(R.id.etUp);
        final EditText etDown = (EditText) itemView.findViewById(R.id.etDown);
        Button btnUp = (Button) itemView.findViewById(R.id.btnUp);
        Button btnDown = (Button) itemView.findViewById(R.id.btnDown);
        Button btnEnd = (Button) itemView.findViewById(R.id.btnEnd);

        tvStation.setText((position + 1) + ". / " + rows.size() + " " + row.getStation());

        etUp.setText(String.valueOf(row.getUp()));
        etDown.setText(String.valueOf(row.getDown()));

        //alapbol beallitunk egy idot, ha ir be erteket akkor felulirjuk
        row.setTime();

        //Elso allomasnal csak felszallok vannak
        if (position == 0) {
            btnDown.setVisibility(View.GONE);
            etDown.setVisibility(View.GONE);
            btnPrev.setVisibility(View.GONE);
        } else {
            btnDown.setVisibility(View.VISIBLE);
            etDown.setVisibility(View.VISIBLE);
            btnPrev.setVisibility(View.VISIBLE);
        }

        //Utolso allomasnal csak leszallok vannak es vege gomb
        if (position == rows.size() - 1) {
            btnUp.setVisibility(View.GONE);
            etUp.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
            btnEnd.setVisibility(View.VISIBLE);
        } else {
            btnUp.setVisibility(View.VISIBLE);
            etUp.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
            btnEnd.setVisibility(View.GONE);
        }



        btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                buttonClick.onPrevClick();

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                buttonClick.onNextClick();

            }
        });


        btnEnd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                buttonClick.onEndClick();

            }
        });

        etUp.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    int up = Integer.valueOf(s.toString());
                    row.setUp(up);
                    row.setTime();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etDown.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    int down = Integer.valueOf(s.toString());
                    row.setDown(down);
                    row.setTime();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Felszallok felfele szamolas
        btnUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try{
                    int up = Integer.valueOf(etUp.getText().toString()) + 1;
                    etUp.setText( String.valueOf(up) );
                    row.setUp(up);
                    row.setTime();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etUp.setText("0");
                }

            }
        });

        //Felszallok lefele szamolas
        btnUp.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                try{
                    if (Integer.valueOf(etUp.getText().toString()) > 0) {
                        int up = Integer.valueOf(etUp.getText().toString()) - 1;
                        etUp.setText(String.valueOf(up));
                        row.setUp(up);
                        row.setTime();
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etUp.setText("0");
                }
                return true;
            }
        });

        //Leszallok felfele szamolas
        btnDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    int down = Integer.valueOf(etDown.getText().toString()) + 1;
                    etDown.setText( String.valueOf(down) );
                    row.setDown(down);
                    row.setTime();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etDown.setText("0");
                }
            }
        });

        //Leszallok lefele szamolas
        btnDown.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                try {
                    if (Integer.valueOf(etDown.getText().toString()) > 0) {
                        int down = Integer.valueOf(etDown.getText().toString()) - 1;
                        etDown.setText(String.valueOf(down));
                        row.setDown(down);
                        row.setTime();
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etDown.setText("0");
                }
                return true;
            }
        });


        container.addView(itemView, 0);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public interface ButtonClick  {
        void onNextClick();
        void onPrevClick();
        void onEndClick();
    }
}

