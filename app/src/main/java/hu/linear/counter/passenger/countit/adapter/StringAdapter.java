package hu.linear.counter.passenger.countit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import hu.linear.counter.passenger.countit.R;

/**
 * Created on 2015.09.14.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class StringAdapter  extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    static class ViewHolder {
        public TextView tvVehicle;
    }

    public StringAdapter(Context context, String[] values) {
        super(context, R.layout.list_row, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_row, parent, false);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tvVehicle = (TextView) rowView.findViewById(R.id.tvVehicle);

            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        String s = values[position];
        holder.tvVehicle.setText(s);

        return rowView;
    }

}
