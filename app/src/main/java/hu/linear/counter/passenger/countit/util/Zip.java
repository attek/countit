package hu.linear.counter.passenger.countit.util;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.File;

/**
 * Jelszoval vedett Zip tomorites es kitomorites
 * szukseges hozza compile files('libs/zip4j_1.3.2.jar')
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class Zip {

    /**
     * Zip file kicsomagolasa
     * @param compressedFile  tomoritett falj
     * @param destination kicsomagolas konybtara
     * @param password jelszo
     */
    public static void decompress(String compressedFile,String destination, String password) {
        try {
            ZipFile zipFile = new ZipFile(compressedFile);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }

        System.out.println("File Decompressed");
    }

    /**
     * File becsomagolasa
     * @param inputFile becsomagolando file
     * @param compressedFile tomoritett file
     * @param password jelszo a titkositashoz, ha ures akkor nincs jelszavas vedelem
     */
    public static void compress(String inputFile, String compressedFile, String password) {
        try {
            ZipFile zipFile = new ZipFile(compressedFile);
            File inputFileH = new File(inputFile);
            ZipParameters parameters = new ZipParameters();

            // COMP_DEFLATE is for compression
            // COMp_STORE no compression
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            // DEFLATE_LEVEL_ULTRA = maximum compression
            // DEFLATE_LEVEL_MAXIMUM
            // DEFLATE_LEVEL_NORMAL = normal compression
            // DEFLATE_LEVEL_FAST
            // DEFLATE_LEVEL_FASTEST = fastest compression
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

            if (password.length() > 0) {

                //Set the encryption flag to true
                parameters.setEncryptFiles(true);

                //Set the encryption method to AES Zip Encryption
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);

                //AES_STRENGTH_128 - For both encryption and decryption
                //AES_STRENGTH_192 - For decryption only
                //AES_STRENGTH_256 - For both encryption and decryption
                //Key strength 192 cannot be used for encryption. But if a zip file already has a
                //file encrypted with key strength of 192, then Zip4j can decrypt this file
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);

                //Set password
                parameters.setPassword(password);

            }

            // file compressed
            zipFile.addFile(inputFileH, parameters);

            long uncompressedSize = inputFileH.length();
            File outputFileH = new File(compressedFile);
            long comrpessedSize = outputFileH.length();

            //System.out.println("Size "+uncompressedSize+" vs "+comrpessedSize);
            double ratio = (double)comrpessedSize/(double)uncompressedSize;
            System.out.println("File compressed with compression ratio : "+ ratio);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
