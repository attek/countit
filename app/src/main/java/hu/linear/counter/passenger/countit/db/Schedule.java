package hu.linear.counter.passenger.countit.db;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import hu.linear.counter.passenger.countit.util.MyApplication;

/**
 * Menetrendi adatok objektuma
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.1
 */
public class Schedule implements Serializable {

	private int id;
    /**
     * Viszonylat
     */
    private String title;
    /**
     * Irany
     */
    private String direction;
    /**
     * Indulas
     */
    private String start;
    /**
     * Viszonylat_nev
     */
    private String destinations;
    /**
     * Sorrend
     */
    private int position;
    /**
     * Megallo_nev
     */
    private String station;

    /**
     * felszallok
     */
    private int up;
    /**
     * leszallok
     */
    private int down;

    /**
     * rogzites idopontja
     */
    private String time;

    /**
     *
     * @param id id
     * @param title Busz szama
     * @param direction Irany(O/V)
     * @param start Indulasi idopont
     * @param destinations viszonlat
     * @param position sorszam
     * @param station megallo
     */
    public Schedule(int id, String title, String direction, String start, String destinations, int position, String station) {
        this.id = id;
        this.title = title;
        this.direction = direction;
        this.start = start;
        this.destinations = destinations.trim();
        this.position = position;
        this.station = station;
    }

    /**
     *
     * @param title Busz szama
     * @param direction Irany(O/V)
     * @param start Indulasi idopont
     * @param destinations viszonlat
     * @param position sorszam
     * @param station megallo
     */
    public Schedule(String title, String direction, String start, String destinations, int position, String station) {
        this.title = title;
        this.direction = direction;
        this.start = start;
        this.destinations = destinations.trim();
        this.position = position;
        this.station = station;
    }

    @Override
    public String toString() {
        /*
        return "Schedule{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", direction='" + direction + '\'' +
                ", start='" + start + '\'' +
                ", destinations='" + destinations + '\'' +
                ", position=" + position +
                ", station='" + station + '\'' +
                '}';
         */
        return title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDestinations() {
        return destinations;
    }

    public void setDestinations(String destinations) {
        this.destinations = destinations;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public int getUp() {
        return up;
    }

    public void setUp(int up) {
        this.up = up;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }

    public String getTime() {
        return time;
    }

    public void setTime() {
        this.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    /**
     * Output a faljba irashoz
     * @return String
     */
    public String getOutput() {

        //viszonylat, irány, indulás, viszonylat név, sorrend,
        // megállónév, vizsgált ajtók száma bitenkénti jelöléssel, fel darab, le darab, dátum (év, hónap, nap).

        return  title + ";" +
                direction + ";" +
                start + ";" +
                destinations + ";" +
                position + ";" +
                station + ";" +
                Integer.parseInt(MyApplication.CHOSEN_DOORS, 2) + ";" +
                up + ";" +
                down + ";" +
                time;


    }

    public String toTest() {

        return "Schedule{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", direction='" + direction + '\'' +
                ", start='" + start + '\'' +
                ", destinations='" + destinations + '\'' +
                ", position=" + position +
                ", station='" + station + '\'' +
                ", up='" + up + '\'' +
                ", down='" + down + '\'' +
                ", time='" + time + '\'' +
                '}';

    }
}
