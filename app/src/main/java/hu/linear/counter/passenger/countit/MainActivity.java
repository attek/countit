package hu.linear.counter.passenger.countit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;

/**
 * Start Screen
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.1
 */
public class MainActivity extends AppCompatActivity implements PopupFragment.IDialogDataPass {

    private EditText etUser;
    private EditText etPassword;
    public static final String TAG = "MainActivity";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        //Ha van mai napi adatbazis akkor nem kell a login kepernyo
        //startHomeScreen();

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        etUser = (EditText) findViewById(R.id.etUser);
        etPassword = (EditText) findViewById(R.id.etPassword);

        MyApplication.setCustomActionbar(this, R.drawable.icon_main, R.string.app_name, false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                login();

            }
        });

        //watching enter press
        etPassword.setOnKeyListener(new EditText.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {

                        login();

                    }
                }

                return false;
            }
        });


    }

    /**
     * Mezok kitoltesenek ellenorzese es API hivas
     */
    private void login() {

        PopupFragment popup = new PopupFragment();
        Bundle b = new Bundle();
        FragmentManager fm = getSupportFragmentManager();

        if (etUser.getText().toString().equals("") || etUser.getText().toString().length() < 5) {

            b.putString("text", getString(R.string.required_user_field));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

        } else if (etPassword.getText().toString().equals("") || etPassword.getText().toString().length() < 5) {

            b.putString("text", getString(R.string.required_password_field));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

        } else {

            checkLogin();

        }
    }


    /**
     * API user ellenorzese, ha valid akkor az alapadatokat elmentjuk
     * a sharedpreferencec-be
     */
    private void checkLogin() {

        if (MyApplication.isOnline(MainActivity.this)) {

            dialog = ProgressDialog.show(MainActivity.this, getString(R.string.login_processing), getString(R.string.please_wait));

            RequestParams params = new RequestParams();

            params.put("AZONOSIT", "");
            params.put("nev", etUser.getText().toString());
            params.put("jelszo", etPassword.getText().toString());

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(MyApplication.API_URL, params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {


                    if (MyApplication.enableLog) Log.d(TAG, new String(response));
                    try {

                        if (MyApplication.enableLog) Log.d(TAG, new String(response));
                        JSONObject jsonObj = new JSONObject(new String(response));

                        JSONObject HCObject = jsonObj.getJSONObject("utasszamlalas");

                        if (HCObject.getString("azonositas").equals("valid")) {

                            SharedPreferences.Editor et = MyApplication.sp.edit();
                            et.putString(MyApplication.USER_NAME, etUser.getText().toString());
                            et.putString(MyApplication.PASSWORD, etPassword.getText().toString());
                            et.putString(MyApplication.ZIP_PASSWORD, HCObject.getString("db_jelszo"));
                            et.putString(MyApplication.MANUAL_URL, HCObject.getString("manual"));
                            et.apply();

                            Intent i = new Intent();

                            i.setClass(MainActivity.this, Home.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();


                        } else {

                            PopupFragment popup = new PopupFragment();
                            Bundle b = new Bundle();
                            FragmentManager fm = getSupportFragmentManager();
                            b.putString("text", getString(R.string.password_error));
                            popup.setArguments(b);
                            popup.show(fm, PopupFragment.TAG);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (dialog.isShowing()) {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                    if (dialog.isShowing()) {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    PopupFragment popup = new PopupFragment();
                    Bundle b = new Bundle();
                    FragmentManager fm = getSupportFragmentManager();
                    b.putString("text", getString(R.string.error_file_upload) + statusCode);
                    popup.setArguments(b);
                    popup.show(fm, PopupFragment.TAG);
                }
            });


        } else {
            /*
            PopupFragment popup = new PopupFragment();
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.login_enable_net));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);*/

            Log.d(TAG, "sp user: " + MyApplication.sp.getString(MyApplication.USER_NAME, ""));
            Log.d(TAG, "sp pass: " + MyApplication.sp.getString(MyApplication.PASSWORD, ""));
            Log.d(TAG, "et user: " + etUser.getText().toString());
            Log.d(TAG, "et pass: " + etPassword.getText().toString());

            if (MyApplication.sp.getString(MyApplication.USER_NAME, "").equals(etUser.getText().toString()) &&
                    MyApplication.sp.getString(MyApplication.PASSWORD, "").equals(etPassword.getText().toString()) ) {

                Intent i = new Intent();
                i.setClass(MainActivity.this, Home.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();


            } else {

                PopupFragment popup = new PopupFragment();
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.wrong_username_or_password));
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }




        }

    }

    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {

    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        //startHomeScreen();

    }

    /**
     * Kezdo oldal inditasa
     */
    private void startHomeScreen() {

        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

        //if (MyApplication.sp.getString(MyApplication.SCHEDULE_DOWNLOAD_DATE, "").equals("")) {

        /*
        if (!MyApplication.sp.getString(MyApplication.USER_NAME, "").equals("") &&
                !MyApplication.sp.getString(MyApplication.PASSWORD, "").equals("") ) {*/

        if (MyApplication.sp.getString(MyApplication.SCHEDULE_DOWNLOAD_DATE, "").equals(currentDate)) {
            Intent i = new Intent();
            i.setClass(MainActivity.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
           //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

}
