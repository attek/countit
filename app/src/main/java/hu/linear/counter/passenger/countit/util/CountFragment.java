package hu.linear.counter.passenger.countit.util;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.adapter.StringAdapter;

/**
 * CountFragment
 *
 * Ajtok szamanak a kivalasztasa
 *
 * Created on 2015.09.14.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class CountFragment extends Fragment {
	
	public static final String TAG = "CountFragment";
	private ArrayList<Integer> doors;
	private SelectItem listener;
	
	public static CountFragment newInstance(Bundle args) {
		CountFragment result = new CountFragment();

		result.setArguments(args);
		
		return result;
	}
	
	
	public void onAttach(Context context) {
	    super.onAttach(context);

        Activity activity;

        if (context instanceof Activity){
            activity = (Activity) context;

            try {
                listener = (SelectItem) activity;
            } catch (ClassCastException ce) {
                Log.e(TAG, "Parent Activity does not implement SelectItem interface!");
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception!");
                e.printStackTrace();
            }

        }


        if (getArguments() != null ) {

            if (getArguments().getSerializable("DATA") instanceof ArrayList ) {
				doors = (ArrayList<Integer>) getArguments().getSerializable("DATA");
            }

        }

	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.choose_fragment, container, false);

        final ListView lvVehicle;

		lvVehicle = (ListView) v.findViewById(R.id.lvVehicle);


        String[] doors = new String[MyApplication.DOORS];
        for(int i = 0; i < MyApplication.DOORS; i++){
            doors[i] = Integer.toString(i + 1);
        }

		StringAdapter mAdapter;
		mAdapter = new StringAdapter(getActivity(), doors);

		lvVehicle.setAdapter(mAdapter);

		lvVehicle.setOnItemClickListener(new OnItemClickListener() {
	       @Override
	       public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {

        	  String listItem = (String) lvVehicle.getItemAtPosition(position);
	          listener.onItemSelected(listItem);

	          FragmentManager fm = getActivity().getSupportFragmentManager();		
	          FragmentTransaction ft = fm.beginTransaction();		
      		        		
	          CountFragment FtChooseFragment = (CountFragment) fm.findFragmentByTag(CountFragment.TAG);
      		        		
	          if (FtChooseFragment != null) {
				ft.hide(FtChooseFragment);
				fm.popBackStack();
	          }
	          ft.commit();
	          
	       } 
	    });
	
		return v;
	}

	
	public interface SelectItem {
		void onItemSelected(String door);
	}


}
