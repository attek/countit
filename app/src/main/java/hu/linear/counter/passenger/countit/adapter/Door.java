package hu.linear.counter.passenger.countit.adapter;

/**
 * Door objektum
 * Created on 2015.09.12.
 *
 * @author Attila Pest
 * @version 1.1
 */
public class Door {
    private int position;
    private boolean selected;

    public Door(int position, boolean selected) {
        this.position = position;
        this.selected = selected;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "Door{" +
                "position=" + position +
                ", selected=" + selected +
                '}';
    }
}
