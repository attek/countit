package hu.linear.counter.passenger.countit;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.io.IOException;

import hu.linear.counter.passenger.countit.util.LogToFile;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;

/**
 * Informacio screen
 * Created on 2015.09.12.
 *
 * @author Attila Pest
 * @version 2.0
 */

public class Info extends AppCompatActivity {

    private ProgressDialog dialog;
    private String manualPath;
    private TextView tvPhone1;
    private TextView tvPhone2;
    private TextView tvPhone3;
    private TextView tvEmail1;
    private TextView tvEmail2;
    private TextView tvEmail3;
    private TextView tvWeb;
    private static final String TAG = "Info";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);


        MyApplication.setCustomActionbar(this, R.drawable.icon_info_head, R.string.title_info, true);

        Button btnManual = (Button) findViewById(R.id.btnManual);
        btnManual.setTransformationMethod(null);

        tvPhone1 = (TextView) findViewById(R.id.tvPhone1);
        tvPhone2 = (TextView) findViewById(R.id.tvPhone2);
        tvPhone3 = (TextView) findViewById(R.id.tvPhone3);

        tvEmail1 = (TextView) findViewById(R.id.tvEmail1);
        tvEmail2 = (TextView) findViewById(R.id.tvEmail2);
        tvEmail3 = (TextView) findViewById(R.id.tvEmail3);
        tvWeb = (TextView) findViewById(R.id.tvWeb);

        tvPhone1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String phoneNumber = tvPhone1.getText().toString().replace(" ","").replace("-", "").replace("Telefon:", "");
                callPhone(phoneNumber);

            }
        });

        tvPhone2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String phoneNumber = tvPhone2.getText().toString().replace(" ","").replace("-", "").replace("Telefon:", "");
                callPhone(phoneNumber);

            }
        });

        tvPhone3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String phoneNumber = tvPhone3.getText().toString().replace(" ","").replace("-", "").replace("Tel.:", "");
                callPhone(phoneNumber);

            }
        });

        tvEmail1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String email = tvEmail1.getText().toString().replace("E-mail: ","");
                String[] addresses = new String[1];
                addresses[0] = email;
                composeEmail(addresses, getString(R.string.app_name));

            }
        });

        tvEmail2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String email = tvEmail2.getText().toString().replace("E-mail: ","");
                String[] addresses = new String[1];
                addresses[0] = email;
                composeEmail(addresses, getString(R.string.app_name));

            }
        });

        tvEmail3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String email = tvEmail3.getText().toString().replace("E-mail: ","");
                String[] addresses = new String[1];
                addresses[0] = email;
                composeEmail(addresses, getString(R.string.app_name));

            }
        });

        tvWeb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String url = tvWeb.getText().toString().replace("| Web: ","");
                Log.d(TAG, "onClick() called with: " + "url = [" + url + "]");
                openUrl(url);

            }
        });

        btnManual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                manualPath = new LogToFile(Info.this).getFolder() + "/" + MyApplication.MANUAL_FILE_NAME;

                if (MyApplication.isOnline(Info.this)) {
                    if (MyApplication.sp.getString(MyApplication.MANUAL_URL, "").equals("")) {

                        PopupFragment popup = new PopupFragment();
                        Bundle b = new Bundle();
                        FragmentManager fm = getSupportFragmentManager();
                        b.putString("text", getString(R.string.no_url_for_manual));
                        popup.setArguments(b);
                        popup.show(fm, PopupFragment.TAG);

                    } else {
                        DownloadManual(MyApplication.sp.getString(MyApplication.MANUAL_URL, ""));
                    }
                } else {

                    openPdf(manualPath);

                }



            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /**
     * Action bar back button
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                Intent i = new Intent();
                i.setClass(Info.this, Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     *
     * @param url Letoltes url
     *
     */
    private void DownloadManual(String url) {

        dialog = ProgressDialog.show(Info.this, getString(R.string.manual), getString(R.string.please_wait));

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(Info.this) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                if (dialog.isShowing()) {
                    try {
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                PopupFragment popup = new PopupFragment();
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.dl_manual_error) + statusCode);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {

                File file = new File(manualPath);
                if (file.delete()) System.out.println("manuale file deleted");

                try {
                    MyApplication.copy(response, file);
                    openPdf(manualPath);

                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("can not copy manual");
                }

                if (dialog.isShowing()) {
                    try {
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

    }


    /**
     *
     * @param path manual path
     */
    private void openPdf(String path) {

        File file = new File(path);
        if (file.isFile()) {
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file), "application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent chooser = Intent.createChooser(target, getString(R.string.open_manual));
            try {
                startActivity(chooser);
            } catch (ActivityNotFoundException e) {

                PopupFragment popup = new PopupFragment();
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.no_pdf_app));
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }
        } else {

            PopupFragment popup = new PopupFragment();
            Bundle b = new Bundle();
            FragmentManager fm = getSupportFragmentManager();
            b.putString("text", getString(R.string.no_internet_no_file));
            popup.setArguments(b);
            popup.show(fm, PopupFragment.TAG);

        }

    }

    /**
     * Call phone number
     * @param phoneNumber Phone number
     */
    private void callPhone(String phoneNumber) {

        Intent call = new Intent(Intent.ACTION_DIAL);
        call.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(call);

    }

    /**
     * Send Email
     * @param addresses Email addresses in array
     * @param subject Email subject
     */
    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void openUrl(String url) {

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);

    }


}

