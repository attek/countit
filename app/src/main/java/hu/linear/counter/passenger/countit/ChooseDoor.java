package hu.linear.counter.passenger.countit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.adapter.Door;
import hu.linear.counter.passenger.countit.adapter.DoorAdapter;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;

/**
 * Szamlalt ajtok kivalasztas az osszes lehetsegesbol
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.4
 */
public class ChooseDoor extends AppCompatActivity implements DoorAdapter.SelectedStatus, PopupFragment.IDialogDataPass {

    private Button btnNext;
    private DoorAdapter mAdapter;
    private ArrayList<Door> doors;
    public static final String TAG = "ChooseDoor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_door);

        MyApplication.setCustomActionbar(this, R.drawable.icon_doors_head, R.string.title_choose_door, false);


        CheckBox cbAllDoor = (CheckBox) findViewById(R.id.cbAllDoor);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        //Szoveg kisbetus legyen
        btnCancel.setTransformationMethod(null);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setTransformationMethod(null);
        GridView gvDoors = (GridView) findViewById(R.id.gvDoors);


        doors = new ArrayList<>();

        //Alap beallitasok
        if (Integer.parseInt(MyApplication.CHOSEN_DOORS, 2) == 0) {
            for (int i = 1; i <= MyApplication.VEHICLE_DOORS; i++) {
                doors.add(new Door(i, false));
            }
            btnNext.setEnabled(false);
        } else {

            String[] doorsArray = MyApplication.CHOSEN_DOORS.split("");

            int j = 1;
            for(int i = MyApplication.VEHICLE_DOORS; i >= 1; i--){
                try {
                    doors.add(new Door(j, doorsArray[i].equals("1")));
                } catch (ArrayIndexOutOfBoundsException e) {
                    doors.add(new Door(j, false));
                }
                j++;
            }

            btnNext.setEnabled(true);
        }

        mAdapter = new DoorAdapter(ChooseDoor.this, doors, ChooseDoor.this);
        gvDoors.setAdapter(mAdapter);

        cbAllDoor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (Door door: doors) {
                        door.setSelected(true);
                    }

                } else {
                    for (Door door: doors) {
                        door.setSelected(false);
                    }
                }

                mAdapter.notifyDataSetChanged();

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                PopupFragment popup = new PopupFragment();
                popup.setListener(ChooseDoor.this);
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.are_you_sure_to_interrupt));
                b.putString("ok", getString(R.string.dialogOk));
                b.putString("cancel", getString(R.string.dialogCancel));
                b.putBoolean("fromCancelButton", true);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(ChooseDoor.this, Start.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onDoorSelect() {

        if (Integer.parseInt(MyApplication.CHOSEN_DOORS, 2) == 0) {
            btnNext.setEnabled(false);
        } else {
            btnNext.setEnabled(true);
        }

    }

    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {

        if (fromCancelButton) {
            finish();
            Intent i = new Intent();
            i.setClass(ChooseDoor.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

    }
}

