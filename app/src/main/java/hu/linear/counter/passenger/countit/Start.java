package hu.linear.counter.passenger.countit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;

/**
 * Szamlalas inditasa elotti osszesito kepernyo
 * Created on 2015.09.15.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class Start extends AppCompatActivity implements PopupFragment.IDialogDataPass {

    public static final String TAG = "Start";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_count);

        MyApplication.setCustomActionbar(this, R.drawable.icon_start, R.string.title_count_start, false);

        Button btnCancel;
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setTransformationMethod(null);

        Button btnStart;
        btnStart = (Button) findViewById(R.id.btnStart);

        TextView tvChosenVehicle;
        TextView tvChosenDirection;
        TextView tvChosenTime;
        TextView tvAllDoors;
        TextView tvWatchedDoors;
        tvChosenVehicle = (TextView) findViewById(R.id.tvChosenVehicle);
        tvChosenDirection = (TextView) findViewById(R.id.tvChosenDirection);
        tvChosenTime = (TextView) findViewById(R.id.tvChosenTime);
        tvAllDoors = (TextView) findViewById(R.id.tvAllDoors);
        tvWatchedDoors = (TextView) findViewById(R.id.tvWatchedDoors);


        if (!MyApplication.CHOSEN_VEHICLE.equals(""))
            tvChosenVehicle.setText(MyApplication.CHOSEN_VEHICLE);
        if (!MyApplication.CHOSEN_DIRECTION.equals(""))
            tvChosenDirection.setText(MyApplication.CHOSEN_DIRECTION);
        if (!MyApplication.CHOSEN_TIME.equals("")) tvChosenTime.setText(MyApplication.CHOSEN_TIME);

        if (MyApplication.VEHICLE_DOORS != 0)
            tvAllDoors.setText(String.valueOf(MyApplication.VEHICLE_DOORS));
        if (!MyApplication.CHOSEN_DOORS.equals("")) tvWatchedDoors.setText(
                MyApplication.CHOSEN_DOORS_BITSET.toString().replace("{", "").replace("}", ""));


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                PopupFragment popup = new PopupFragment();
                popup.setListener(Start.this);
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.are_you_sure_to_interrupt));
                b.putString("ok", getString(R.string.dialogOk));
                b.putString("cancel", getString(R.string.dialogCancel));
                b.putBoolean("fromCancelButton" , true);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(Start.this, CountActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {
        if (fromCancelButton) {
            finish();
            Intent i = new Intent();
            i.setClass(Start.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

    }
}

