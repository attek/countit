package hu.linear.counter.passenger.countit.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.BitSet;

import hu.linear.counter.passenger.countit.R;

/**
 * Alapbeallitasok, allandok
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.0
 */
@ReportsCrashes(
        //mailTo = "pest.attila@gmail.com",
        formUri = "http://pestattila.com/hc_arca/",
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class MyApplication extends Application {

    public static final String TAG = "MyApplication";
    public static String CHOSEN_VEHICLE = "";
    public static String CHOSEN_DIRECTION = "";
    public static String CHOSEN_TIME = "";
    public static String CHOSEN_CODE = "";
    public static String CHOSEN_DOORS = "0";
    public static BitSet CHOSEN_DOORS_BITSET = new BitSet();
    public static int VEHICLE_DOORS = 0;

    //public static String API_URL = "http://w2.linear.hu:8080/MVK_utasszamlalo/handler_JSon.php";
    public static String API_URL = "http://valosidoben.mvkzrt.hu:8080/MVK_utasszamlalo/handler_JSon.php";

    /**
     * Adott kozlekedesi vallalat legtobb ajtoval rendelkezo jarmu ajto szama
     */
    public static final int DOORS = 6;
    /**
     * Enable header authentication
     */
    public static final boolean headerAuth = false;

    /**
     * Authentication details
     * base64 encode username:password
     */
    public static final String API_AUTH = "";

    public static final String SCHEDULE_FILE_NAME = "schedule.zip";
    public static final String EXTRACT_FILE_NAME = "adatok.txt";
    public static final String MANUAL_FILE_NAME = "mvk_hasznalati_utmutato.pdf";
    //for sharedpreferences
    public static final String SCHEDULE_DOWNLOAD_DATE = "SCHEDULE_DOWNLOAD_DATE";
    public static final String USER_NAME = "USER_NAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String ZIP_PASSWORD = "ZIP_PASSWORD";
    public static final String MANUAL_URL = "MANUAL_URL";

    public static boolean enableLog = false;

    public static SharedPreferences sp;
    public static int SDK = 0;
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static int SCREEN_X_DPI;
    public static int SCREEN_Y_DPI;
    public static DisplayMetrics metrics;

    @Override
    public void onCreate() {
        super.onCreate();
        sp = getSharedPreferences("DB_INIT", MODE_PRIVATE);

        SDK = android.os.Build.VERSION.SDK_INT;

        metrics = getResources().getDisplayMetrics();

        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        SCREEN_X_DPI = (int) metrics.xdpi;
        SCREEN_Y_DPI = (int) metrics.ydpi;

        if (enableLog) Log.d(TAG, "x dpi: " + SCREEN_X_DPI + " y dpi: " + SCREEN_Y_DPI);
        if (enableLog) Log.d(TAG, "screen width dpi: " + SCREEN_WIDTH + " screen height: " + SCREEN_HEIGHT);

        ACRA.init(this);
    }

    /**
     *
     * @param ctx Context
     * @param iconId icon
     * @param titleId title
     * @param backButton show back button
     */
    public static void setCustomActionbar(Context ctx, int iconId, int titleId, boolean backButton) {

        //Set custom action bar
        LayoutInflater mInflater = LayoutInflater.from(ctx);
        View mActionbarView = mInflater.inflate(R.layout.action_bar, null);
        TextView tvActionBarTitle = (TextView) mActionbarView.findViewById(R.id.tvActionBarTitle);
        ImageView ivActionBarIcon = (ImageView) mActionbarView.findViewById(R.id.ivActionBarIcon);

        tvActionBarTitle.setText(titleId);
        ivActionBarIcon.setImageDrawable(ContextCompat.getDrawable(ctx, iconId));

        AppCompatActivity activity = (AppCompatActivity) ctx;
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(mActionbarView);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

            if (backButton) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    /**
     *
     * @param context Context
     * @return boolean
     */
    static public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnected();

    }

    /**
     * Szoveg atalakitasa ekezet es specialis karakter nelkuli
     * file nevve
     * @param string Atalakitando szoveg
     * @return String
     */
    public static String convertToFileName(String string) {

        String text = string.toLowerCase();
        StringBuilder sb=new StringBuilder();

        char[] c= text.toCharArray();

        for (char aC : c) {
            switch (aC) {
                case 'á':
                    sb.append("a");
                    break;
                case 'Á':
                    sb.append("A");
                    break;
                case 'é':
                    sb.append("e");
                    break;
                case 'É':
                    sb.append("E");
                    break;
                case 'í':
                    sb.append("i");
                    break;
                case 'Í':
                    sb.append("I");
                    break;
                case 'ó':
                    sb.append("o");
                    break;
                case 'Ó':
                    sb.append("O");
                    break;
                case 'ö':
                    sb.append("o");
                    break;
                case 'Ö':
                    sb.append("O");
                    break;
                case 'ő':
                    sb.append("o");
                    break;
                case 'Ő':
                    sb.append("O");
                    break;
                case 'ú':
                    sb.append("u");
                    break;
                case 'Ú':
                    sb.append("U");
                    break;
                case 'ü':
                    sb.append("u");
                    break;
                case 'Ü':
                    sb.append("U");
                    break;
                case 'ű':
                    sb.append("u");
                    break;
                case 'Ű':
                    sb.append("U");
                    break;
                default:
                    sb.append(aC);
            }
        }

        //string = sb.toString().replaceAll("[^a-z0-9.-]", "_");
        string = sb.toString().replaceAll("[^a-z0-9.-]", "-");

        return string;

    }


    /**
     * Copy file
     * @param src source File
     * @param dst destination File
     * @throws IOException
     */
    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

}
