package hu.linear.counter.passenger.countit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.adapter.Type;
import hu.linear.counter.passenger.countit.db.Schedule;
import hu.linear.counter.passenger.countit.db.ScheduleDataSource;
import hu.linear.counter.passenger.countit.util.ChooseFragment;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;


/**
 * Kivalasztas screen
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.5
 */
public class Choose extends AppCompatActivity implements ChooseFragment.SelectItem, PopupFragment.IDialogDataPass {

    public static final int VEHICLE_CODE_LENGTH = 4;
    private Button btnNext;
    private EditText etCode;
    private Button btnDirection;
    private Button btnTime;
    private TextView tvChosenVehicle;
    private TextView tvChosenDirection;
    private TextView tvChosenTime;

    private Schedule chosenVehicle;
    private Schedule chosenDestination;
    private Schedule chosenTime;

    private ScheduleDataSource dataSource;

    private ArrayList<Schedule> vehicles;
    private ArrayList<Schedule> destinations;
    private ArrayList<Schedule> times;

    public static final String TAG = "Choose";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose);

        MyApplication.setCustomActionbar(this, R.drawable.icon_vehicle_head, R.string.title_choose, false);

        Button btnCancel;
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setTransformationMethod(null);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setTransformationMethod(null);

        etCode = (EditText) findViewById(R.id.etCode);
        final Button btnVehicle;
        btnVehicle = (Button) findViewById(R.id.btnVehicle);
        btnDirection = (Button) findViewById(R.id.btnDirection);
        btnTime = (Button) findViewById(R.id.btnTime);

        tvChosenVehicle = (TextView) findViewById(R.id.tvChosenVehicle);
        tvChosenDirection = (TextView) findViewById(R.id.tvChosenDirection);
        tvChosenTime = (TextView) findViewById(R.id.tvChosenTime);

        btnNext.setEnabled(false);

        dataSource = new ScheduleDataSource(Choose.this);
        dataSource.open();

        vehicles = new ArrayList<>();
        vehicles = (ArrayList<Schedule>) dataSource.getScheduleTitles();

        if (vehicles.size() > 0 && etCode.getText().length() == VEHICLE_CODE_LENGTH) btnVehicle.setEnabled(true);
        else btnVehicle.setEnabled(false);

        destinations = new ArrayList<>();
        times  = new ArrayList<>();

        btnDirection.setEnabled(false);
        btnTime.setEnabled(false);

        enableNextButton();

        btnVehicle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                hideSoftKeyboard();

                tvChosenDirection.setText("");
                btnTime.setEnabled(false);
                tvChosenTime.setText("");

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                Bundle b = new Bundle();
                b.putSerializable("DATA", vehicles);
                b.putSerializable("TYPE", Type.VEHICLE);
                ft.add(R.id.rlChoose, ChooseFragment.newInstance(b), ChooseFragment.TAG);
                ft.addToBackStack(null);

                ft.commit();
            }
        });

        btnDirection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                hideSoftKeyboard();

                btnTime.setEnabled(false);
                tvChosenTime.setText("");

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                Bundle b = new Bundle();
                b.putSerializable("DATA", destinations);
                b.putSerializable("TYPE", Type.DESTINATION);
                ft.add(R.id.rlChoose, ChooseFragment.newInstance(b), ChooseFragment.TAG);
                ft.addToBackStack(null);

                ft.commit();

            }
        });

        btnTime.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                hideSoftKeyboard();

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                Bundle b = new Bundle();
                b.putSerializable("DATA", times);
                b.putSerializable("TYPE", Type.TIME);
                ft.add(R.id.rlChoose, ChooseFragment.newInstance(b), ChooseFragment.TAG);
                ft.addToBackStack(null);

                ft.commit();

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                PopupFragment popup = new PopupFragment();
                popup.setListener(Choose.this);
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.are_you_sure_to_interrupt));
                b.putString("ok", getString(R.string.dialogOk));
                b.putString("cancel", getString(R.string.dialogCancel));
                b.putBoolean("fromCancelButton", true);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(Choose.this, DoorsCount.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        /**
         * Code beirasanak ellenorzese
         */
        /* Alertdialog van helyette
        etCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                enableNextButton();
                if (s.length() < VEHICLE_CODE_LENGTH) {
                    btnVehicle.setEnabled(false);
                    btnDirection.setEnabled(false);
                    btnTime.setEnabled(false);
                } else if (vehicles.size() > 0) {
                    btnVehicle.setEnabled(true);
                    hideSoftKeyboard();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        */

        etCode.setFocusable(false);

        etCode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(Choose.this);

                LayoutInflater inflater = Choose.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_dialog_choose, null);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();

                final EditText etCodeDialog = (EditText) dialogView.findViewById(R.id.etCode);
                Button btnReady = (Button) dialogView.findViewById(R.id.btnReady);

                etCodeDialog.setText(etCode.getText());
                etCodeDialog.setSelection(etCodeDialog.getText().length());

                etCodeDialog.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        if (s.length() == VEHICLE_CODE_LENGTH) {
                            hideDialogSoftKeyboard(alertDialog);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                btnReady.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (etCodeDialog.getText().length() == VEHICLE_CODE_LENGTH) {
                            etCode.setText(etCodeDialog.getText());
                            hideSoftKeyboard();
                            enableNextButton();
                            if (!btnVehicle.isEnabled()) btnVehicle.setEnabled(true);
                            alertDialog.dismiss();
                        } else {
                            etCodeDialog.setError(getString(R.string.vechicle_code_length));
                        }
                    }
                });

                alertDialog.show();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!MyApplication.CHOSEN_VEHICLE.equals("")) {
            chosenVehicle = dataSource.getScheduleByTitle(MyApplication.CHOSEN_VEHICLE);
            tvChosenVehicle.setText(MyApplication.CHOSEN_VEHICLE);
            destinations = (ArrayList<Schedule>) dataSource.getScheduleDirections(chosenVehicle);
            if (destinations.size() > 0) btnDirection.setEnabled(true);
            else btnDirection.setEnabled(false);
        }

        if (!MyApplication.CHOSEN_CODE.equals("")) {
            etCode.setText(MyApplication.CHOSEN_CODE);
            hideSoftKeyboard();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataSource.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    @Override
    public void onItemSelected(Schedule schedule, Type type) {

        switch (type) {
            case VEHICLE:
                tvChosenVehicle.setText(schedule.getTitle());

                tvChosenDirection.setText("");
                tvChosenTime.setText("");

                chosenVehicle = schedule;
                destinations = (ArrayList<Schedule>) dataSource.getScheduleDirections(chosenVehicle);
                if (destinations.size() > 0) btnDirection.setEnabled(true);
                else btnDirection.setEnabled(false);

                break;
            case DESTINATION:
                tvChosenDirection.setText(schedule.getDestinations());

                tvChosenTime.setText("");

                chosenDestination = schedule;
                times = (ArrayList<Schedule>) dataSource.getScheduleTimes(chosenDestination);
                if (times.size() > 0) {
                    btnTime.setEnabled(true);
                } else {
                    btnTime.setEnabled(false);

                    PopupFragment popup = new PopupFragment();
                    Bundle b = new Bundle();
                    FragmentManager fm = getSupportFragmentManager();
                    b.putString("text", getString(R.string.no_start_time));
                    popup.setArguments(b);
                    popup.show(fm, PopupFragment.TAG);

                }

                break;
            case TIME:
                tvChosenTime.setText(schedule.getStart());
                chosenTime = schedule;
                break;
            default:
                break;
        }

        enableNextButton();
    }

    /**
     * Kovetkezo gomb engedelyezesenek ellenorzese
     */
    public void enableNextButton() {

        if (!tvChosenVehicle.getText().equals("") &&
                !tvChosenDirection.getText().equals("") &&
                !tvChosenTime.getText().equals("") &&
                etCode.getText().length() == VEHICLE_CODE_LENGTH
                ) {
            btnNext.setEnabled(true);

            MyApplication.CHOSEN_VEHICLE = tvChosenVehicle.getText().toString();
            MyApplication.CHOSEN_DIRECTION = tvChosenDirection.getText().toString();
            MyApplication.CHOSEN_TIME = tvChosenTime.getText().toString();
            MyApplication.CHOSEN_CODE = etCode.getText().toString();

        } else {
            btnNext.setEnabled(false);
        }
    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void hideDialogSoftKeyboard(AlertDialog dialog) {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(dialog.getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {

        if (fromCancelButton) {
            finish();
            Intent i = new Intent();
            i.setClass(Choose.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

    }
}

