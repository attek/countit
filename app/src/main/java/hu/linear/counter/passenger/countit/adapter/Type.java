package hu.linear.counter.passenger.countit.adapter;

/**
 * Created on 2015.09.13.
 *
 * @author Attila Pest
 * @version 1.0
 */
public enum  Type {
    /**
     * Jarmu, irany, idopont
     */
    VEHICLE, DIRECTION, TIME, DESTINATION
}
