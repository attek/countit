package hu.linear.counter.passenger.countit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.util.MyApplication;

/**
 * Created on 2015.09.12.
 *
 * @author Attila Pest
 * @version 1.4
 */
public class DoorAdapter extends BaseAdapter {
    private final ArrayList<Door> rows;
    private Context ctx;
    private SelectedStatus selectedStatus;

    public DoorAdapter(final Context context, final ArrayList<Door> aRows, SelectedStatus aListener ) {
        rows = aRows;
        ctx = context;
        selectedStatus = aListener;
    }

    public int getCount() {
        return rows.size();
    }

    public void add(Door aDoor) {
        rows.add(aDoor);
    }

    public Object getItem(int position) {
        return rows.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final Door row = rows.get(position);

        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.grid_row, parent, false);
            holder = new ViewHolder();

            holder.cbDoor = (CheckBox) convertView.findViewById(R.id.cbDoor);

            convertView.setTag(holder); // setting Holder as arbitrary object for row

        } else {

            // row already contains Holder object
            holder = (ViewHolder) convertView.getTag();

        }

        //Resources res = ctx.getResources();

        holder.cbDoor.setText(String.valueOf(row.getPosition()));

        if (row.isSelected()) {
            holder.cbDoor.setChecked(true);
        } else {
            holder.cbDoor.setChecked(false);
        }

        //Click events
        holder.cbDoor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    row.setSelected(true);
                } else {
                    row.setSelected(false);
                }

                MyApplication.CHOSEN_DOORS = chosenDoors();
                selectedStatus.onDoorSelect();

            }
        });


        return convertView;
    }

    /**
     * ViewHolder design pattern
     */
    static class ViewHolder {

        CheckBox cbDoor;

    }

    /**
     * Kivalasztott ajtok binarisan, helyiertek helyesen
     * @return String chosenDoors
     */
    private String chosenDoors() {

        String chosenDoors = "";

        for (Door door: rows) {
            chosenDoors = (door.isSelected() ? "1" : "0") + chosenDoors;
            MyApplication.CHOSEN_DOORS_BITSET.set(door.getPosition(), door.isSelected());
        }

        return chosenDoors;

    }

    /**
     * Interface ChooseDoor activityhez, az allapot valtozasanak jelzesere
     */
    public interface SelectedStatus  {
        void onDoorSelect();
    }
}
