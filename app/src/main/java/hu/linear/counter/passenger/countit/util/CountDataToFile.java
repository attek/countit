package hu.linear.counter.passenger.countit.util;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * Szamolasi adatok file-ban rozgitese
 * becsomagolasa jelszo vedelemmel
 *
 * Created on 2015.09.17.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class CountDataToFile {

    private File logDir;
    private File logFile;
    private String fileName;
    private static final String TAG = "CountDataToFile";

    public CountDataToFile(Context context) {


        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            logDir = new File(android.os.Environment.getExternalStorageDirectory(), "countit" + File.separator + "upload");
        } else {
            logDir = context.getCacheDir();
        }

        if (!logDir.exists()) {
            if (!logDir.mkdirs()) Log.e(TAG, "Could not create countit/upload directory!");
        }

    }

    public CountDataToFile(Context context, String filename) {

        this.fileName = filename;

        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            logDir = new File(android.os.Environment.getExternalStorageDirectory(), "countit" + File.separator + "upload");
        } else {
            logDir = context.getCacheDir();
        }

        if (!logDir.exists()) {
            if (!logDir.mkdirs()) Log.e(TAG, "Could not create countit/upload directory!");
        }

        logFile = new File(logDir + "/" + filename);

        if (!logFile.exists()) {
            try {
                if (!logFile.createNewFile()) Log.e(TAG, "Could not create " + filename + " file!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * getFolder()
     * Get folder
     *
     * @return String path
     */
    public String getFolder() {

        return logDir.getAbsolutePath();

    }

    /**
     * Append string to log file with timestamp
     *
     * @param text egy szamolasi adat sor
     */
    public void appendData(String text) {

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));

            buf.append(text);
            //buf.newLine();
            buf.write("\r\n");
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Falj tomoritese jelszovedelemmel, eredeti file torlese
     */
    public void compressFile() {

        String path = getFolder() + File.separator + fileName;
        String decompress_path = path.replace(".txt", ".zip");
        Zip.compress(path, decompress_path, MyApplication.sp.getString(MyApplication.ZIP_PASSWORD, ""));

        if (!logFile.delete()) Log.e(TAG, "Could not delete " + path + " file!");

    }

}