package hu.linear.counter.passenger.countit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hu.linear.counter.passenger.countit.util.CountFragment;
import hu.linear.counter.passenger.countit.util.MyApplication;
import hu.linear.counter.passenger.countit.util.PopupFragment;

/**
 * Jarmuvon levo ajtok szamanak beallitasa
 * Created on 2015.09.14.
 *
 * @author Attila Pest
 * @version 1.3
 */
public class DoorsCount extends AppCompatActivity implements CountFragment.SelectItem, PopupFragment.IDialogDataPass {

    private Button btnNext;
    private TextView tvChosenDoorsCount;
    //public static final String TAG = "DoorsCount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doors_count);

        MyApplication.setCustomActionbar(this, R.drawable.icon_doors_head, R.string.title_choose_door, false);

        Button btnCancel;
        btnCancel = (Button) findViewById(R.id.btnCancel);
        //Szoveg kisbetus legyen
        btnCancel.setTransformationMethod(null);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setTransformationMethod(null);
        btnNext.setEnabled(false);

        Button btnDoorsCount;
        btnDoorsCount = (Button) findViewById(R.id.btnDoorsCount);
        tvChosenDoorsCount = (TextView) findViewById(R.id.tvChosenDoorsCount);

        btnDoorsCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                Bundle b = new Bundle();
                ft.add(R.id.rlChoose, CountFragment.newInstance(b), CountFragment.TAG);
                ft.addToBackStack(null);

                ft.commit();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                PopupFragment popup = new PopupFragment();
                popup.setListener(DoorsCount.this);
                Bundle b = new Bundle();
                FragmentManager fm = getSupportFragmentManager();
                b.putString("text", getString(R.string.are_you_sure_to_interrupt));
                b.putString("ok", getString(R.string.dialogOk));
                b.putString("cancel", getString(R.string.dialogCancel));
                b.putBoolean("fromCancelButton", true);
                popup.setArguments(b);
                popup.show(fm, PopupFragment.TAG);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(DoorsCount.this, ChooseDoor.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        //Elozoleg beallitott ajtok szamanak kiiratasa
        if (MyApplication.VEHICLE_DOORS != 0 ) {
            tvChosenDoorsCount.setText(Integer.toString(MyApplication.VEHICLE_DOORS));
            btnNext.setEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    @Override
    public void onItemSelected(String door) {
        tvChosenDoorsCount.setText(door);
        btnNext.setEnabled(true);
        MyApplication.VEHICLE_DOORS = Integer.parseInt(door);
    }

    @Override
    public void onPositiveAnswer(boolean fromCancelButton) {
        if (fromCancelButton) {
            finish();
            Intent i = new Intent();
            i.setClass(DoorsCount.this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    public void onNegativeAnswer(boolean fromCancelButton) {

    }
}

