package hu.linear.counter.passenger.countit.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.db.Schedule;
import hu.linear.counter.passenger.countit.db.ScheduleDataSource;

/**
 * Menetrend felodolgozasa a hatterben
 * Letoltott Zip falj kitomoritese, csv feldolgozasa,
 * majd adatbazisba toltese, adatbazis muvelet utan a letoltott es
 * a kitomoritett falj toroljuk
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class ProcessSchedule extends AsyncTask<String, Void, String> {

    private Context context = null;
    ProgressDialog dialog;
    boolean showProgressDialog = false;

    /**
     *
     * @param context Context
     * @param aShowProgressDialog Show progress dialog
     */
    public ProcessSchedule(Context context, boolean aShowProgressDialog) {
        this.context = context;
        showProgressDialog = aShowProgressDialog;
    }

    @Override
    protected void onPreExecute() {
        if (showProgressDialog) {

            dialog = ProgressDialog.show(context, context.getString(R.string.schedule_processing), context.getString(R.string.please_wait));
        }
    }

    @Override
    protected String doInBackground(String... params) {

        String path = new LogToFile(context).getFolder() + "/" + MyApplication.SCHEDULE_FILE_NAME;
        String decompress_path = new LogToFile(context).getFolder() + "/decompress";

        File file = new File(decompress_path + "/" + MyApplication.EXTRACT_FILE_NAME);

        Zip.decompress(path, decompress_path, MyApplication.sp.getString(MyApplication.ZIP_PASSWORD, ""));

        CSVReader reader;
        try {

            List<Schedule> Schedules = new ArrayList<>();

            //http://opencsv.sourceforge.net/
            InputStreamReader csvStreamReader = new InputStreamReader(new FileInputStream(file));
            reader = new CSVReader(csvStreamReader, ';');

            String[] nextLine;

            int i = 0;
            while ((nextLine = reader.readNext()) != null) {
                if (i > 0) {
                    Schedules.add(new Schedule(nextLine[0], nextLine[1], nextLine[2], nextLine[3], Integer.parseInt(nextLine[4]), nextLine[5]));
                }

                i++;
            }

            ScheduleDataSource dataSource =  new ScheduleDataSource(context);
            dataSource.open();
            dataSource.deleteAllSchedule();
            dataSource.bulkInsertSchedule(Schedules);
            dataSource.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        if (showProgressDialog) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //kitomoritett es feldolgozott file torlese
        String txt_path = new LogToFile(context).getFolder() + "/decompress/" + MyApplication.EXTRACT_FILE_NAME;
        File file = new File(txt_path);
        if (file.delete()) System.out.println("decopressed " + MyApplication.EXTRACT_FILE_NAME + " file deleted");

        //zip falj torlese
        String path = new LogToFile(context).getFolder() + "/" + MyApplication.SCHEDULE_FILE_NAME;
        File file_zip = new File(path);
        if (file_zip.delete()) System.out.println("processed " +  MyApplication.SCHEDULE_FILE_NAME + " file deleted");

    }


}
