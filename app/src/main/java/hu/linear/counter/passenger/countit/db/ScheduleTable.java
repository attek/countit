package hu.linear.counter.passenger.countit.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


/**
 * Viszonylatok adatbazis tablaja
 * Created on 2015.08.30.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class ScheduleTable {

    public static final String TABLE_NAME = "schedule";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DIRECTION = "direction";
    public static final String COLUMN_START = "start";
    public static final String COLUMN_DESTINATION = "destinations";
    public static final String COLUMN_POS = "position";
    public static final String COLUMN_STATION = "station";

    private static final String DATABASE_CREATE = "create table " + TABLE_NAME
            + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TITLE + " text not null, "
            + COLUMN_DIRECTION + " text not null, "
            + COLUMN_START + " text not null, "
            + COLUMN_DESTINATION + " text not null, "
            + COLUMN_POS + " integer, "
            + COLUMN_STATION + " text not null "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(hu.linear.counter.passenger.countit.db.ScheduleTable.class.getName(), "Upgrading from version " + oldVersion
                + " to " + newVersion);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
