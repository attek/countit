package hu.linear.counter.passenger.countit.util;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hu.linear.counter.passenger.countit.R;

/**
 * Alert dialog uzenetek megjelenitesere
 * Created on 2015.09.07.
 *
 * @author Attila Pest
 * @version 1.1
 */
public class PopupFragment extends DialogFragment {

    // Log tag
    public static final String TAG = "FtPopup";

    // UI
    private TextView dialogTitle;
    private TextView dialogText;
    private TextView dialogOk;
    private TextView dialogCancel;
    private ImageView ivAlertIcon;
    private LinearLayout llHeader;
    private ImageView ivMVK;
    private boolean fromCancelButton = false;


    private IDialogDataPass listener = null;

    public PopupFragment() {
        // Empty constructor required for DialogFragment
    }

    public void setListener(IDialogDataPass aListener) {
        listener = aListener;
    }


    public void onAttach(Activity activity) {
        super.onAttach(activity);


        if (getTargetFragment() != null) {
            try {
                listener = (IDialogDataPass) getTargetFragment();
                //getTargetFragment().getTargetRequestCode();

            } catch (ClassCastException ce) {
                Log.e(TAG,
                        "Target Fragment does not implement fragment interface!");
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception!");
                e.printStackTrace();
            }
        } else {

            if (listener == null) {

                try {
                    listener = (IDialogDataPass) activity;
                } catch (ClassCastException ce) {
                    Log.e(TAG, "Parent Activity does not implement IDialogDataPass interface!");
                } catch (Exception e) {
                    Log.e(TAG, "Unhandled exception!");
                    e.printStackTrace();
                }
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.alert_dialog, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        // request a window without the title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        // View references
        dialogTitle = (TextView) root.findViewById(R.id.dialogTitle);
        dialogText = (TextView) root.findViewById(R.id.dialogText);
        dialogOk = (TextView) root.findViewById(R.id.dialogOk);
        dialogCancel = (TextView) root.findViewById(R.id.dialogCancel);

        ivAlertIcon = (ImageView) root.findViewById(R.id.ivAlertIcon);
        llHeader = (LinearLayout) root.findViewById(R.id.llHeader);
        ivMVK = (ImageView) root.findViewById(R.id.ivMVK);

        dialogCancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                listener.onNegativeAnswer(fromCancelButton);
                dismiss();
            }
        });

        dialogOk.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                listener.onPositiveAnswer(fromCancelButton);
                dismiss();
            }
        });

        return root;
    }

    public void onStart() {
        if (getArguments() != null) {

            dialogTitle.setText(getArguments().getString("title"));
            dialogText.setText(getArguments().getString("text"));

            if (getArguments().getString("ok") != null) {
                dialogOk.setVisibility(View.VISIBLE);
                dialogOk.setText(getArguments().getString("ok"));
            }

            if (getArguments().getString("cancel") != null) {
                dialogCancel.setVisibility(View.VISIBLE);
                dialogCancel.setText(getArguments().getString("cancel"));
            }

            if (getArguments().getString("logout") != null) {

                ivMVK.setVisibility(View.VISIBLE);
                llHeader.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.button_blue));
                ivAlertIcon.setImageResource(R.drawable.icon_exit);

            }

            if (getArguments().getBoolean("fromCancelButton")) {

                fromCancelButton = getArguments().getBoolean("fromCancelButton");
            }

        }
        super.onStart();
    }


    // Listener interface
    public interface IDialogDataPass {
        void onPositiveAnswer(boolean fromCancelButton);

        void onNegativeAnswer(boolean fromCancelButton);
    }


}