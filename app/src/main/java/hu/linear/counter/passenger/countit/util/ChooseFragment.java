package hu.linear.counter.passenger.countit.util;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.adapter.ScheduleAdapter;
import hu.linear.counter.passenger.countit.adapter.Type;
import hu.linear.counter.passenger.countit.db.Schedule;

/**
 * ChooseFragment
 *
 * Listview Fragment, jarmu, viszonylat es ido valasztasara
 * Adatbazis muvelet Choose.java-ban tortenik, ide csak atadjuk az ertekeket
 *
 * Created on 2015.09.13.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class ChooseFragment extends Fragment {
	
	public static final String TAG = "ChooseFragment";
	private ArrayList<Schedule> vehicles;
    private Type type;
	private SelectItem listener;
	
	public static ChooseFragment newInstance(Bundle args) {
		ChooseFragment result = new ChooseFragment();

		result.setArguments(args);
		
		return result;
	}
	
	
	public void onAttach(Context context) {
	    super.onAttach(context);

        Activity activity;

        if (context instanceof Activity){
            activity = (Activity) context;

            try {
                listener = (SelectItem) activity;
            } catch (ClassCastException ce) {
                Log.e(TAG, "Parent Activity does not implement SelectItem interface!");
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception!");
                e.printStackTrace();
            }

        }


        if (getArguments() != null ) {

            if (getArguments().getSerializable("DATA") instanceof ArrayList ) {
                vehicles = (ArrayList<Schedule>) getArguments().getSerializable("DATA");
            }

            type = (Type) getArguments().getSerializable("TYPE");

        }

	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.choose_fragment, container, false);

        final ListView lvVehicle;
        ScheduleAdapter mAdapter;

		lvVehicle = (ListView) v.findViewById(R.id.lvVehicle);

		mAdapter = new ScheduleAdapter(getActivity(), vehicles, type);

		lvVehicle.setAdapter(mAdapter);

		lvVehicle.setOnItemClickListener(new OnItemClickListener() {
	       @Override
	       public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {

        	  Schedule listItem = (Schedule) lvVehicle.getItemAtPosition(position);
	          listener.onItemSelected(listItem, type);

	          FragmentManager fm = getActivity().getSupportFragmentManager();		
	          FragmentTransaction ft = fm.beginTransaction();		
      		        		
	          ChooseFragment FtChooseFragment = (ChooseFragment) fm.findFragmentByTag(ChooseFragment.TAG);
      		        		
	          if (FtChooseFragment != null) {
				ft.hide(FtChooseFragment);
				fm.popBackStack();
	          }
	          ft.commit();
	          
	       } 
	    });
	
		return v;
	}

	
	public interface SelectItem {
		void onItemSelected(Schedule schedule, Type type);
	}


}
