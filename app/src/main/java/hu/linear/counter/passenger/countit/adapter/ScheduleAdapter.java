package hu.linear.counter.passenger.countit.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hu.linear.counter.passenger.countit.R;
import hu.linear.counter.passenger.countit.db.Schedule;

/**
 * Created on 2015.09.13.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class ScheduleAdapter extends BaseAdapter {
    private final ArrayList<Schedule> rows;
    private Context ctx;
    private Type type;

    public ScheduleAdapter(final Context context, final ArrayList<Schedule> aRows, Type aType ) {
        rows = aRows;
        ctx = context;
        type = aType;
    }

    public int getCount() {
        return rows.size();
    }

    public void add(Schedule aSchedule) {
        rows.add(aSchedule);
    }

    public Object getItem(int position) {
        return rows.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final Schedule row = rows.get(position);

        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_row, parent, false);
            holder = new ViewHolder();

            holder.tvVehicle = (TextView) convertView.findViewById(R.id.tvVehicle);

            convertView.setTag(holder); // setting Holder as arbitrary object for row

        } else {

            // row already contains Holder object
            holder = (ViewHolder) convertView.getTag();

        }

        Resources res = ctx.getResources();

        switch (type) {
            case VEHICLE:
                holder.tvVehicle.setText(String.valueOf(row.getTitle()));
                break;
            case DESTINATION:
                holder.tvVehicle.setText(String.valueOf(row.getDestinations()));
                break;
            case TIME:
                holder.tvVehicle.setText(String.valueOf(row.getStart()));
                break;
            default:
                break;
        }


        //Click events handle on ChooseFragment

        return convertView;
    }

    /**
     * ViewHolder design pattern
     */
    static class ViewHolder {

        TextView tvVehicle;

    }


}
