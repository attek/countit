package hu.linear.counter.passenger.countit.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.util.Log;

/**
 *
 * Log text to file
 * usage:
 * LogToFile logToFile = new LogToFile(context);
 * logToFile.appendLog("Log some text....");
 *
 * Created on 2015.09.02.
 *
 * @author Attila Pest
 * @version 1.0
 */
public class LogToFile {

    private File logDir;
    private static final String TAG = "LogToFile";

    public LogToFile(Context context) {

        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            logDir = new File(android.os.Environment.getExternalStorageDirectory(), "countit");
        } else {
            logDir = context.getCacheDir();
        }

        if (!logDir.exists()) {
            if (!logDir.mkdirs()) Log.e(TAG, "Could not create countit directory!");
        }
    }

    /**
     * getFolder()
     * Get folder
     *
     * @return String path
     */
    public String getFolder() {

        return logDir.getAbsolutePath();

    }

    /**
     * Append string to log file with timestamp
     *
     * @param text Logolasra szant szoveg
     */
    public void appendLog(String text) {
        File logFile = new File(getFolder() + "/contit.log");
        if (!logFile.exists()) {
            try {
                if (!logFile.createNewFile()) Log.e(TAG, "Could not create countit.log file!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));

            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            text = timeStamp + " " + text;
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}