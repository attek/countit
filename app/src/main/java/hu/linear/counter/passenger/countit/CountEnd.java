package hu.linear.counter.passenger.countit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import hu.linear.counter.passenger.countit.util.MyApplication;

/**
 * Szamlalas vege kepernyo
 * Created on 2015.09.17.
 *
 * @author Attila Pest
 * @version 1.0
 */

public class CountEnd extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.count_end);

        MyApplication.setCustomActionbar(this, R.drawable.icon_end, R.string.title_count_end, false);

        Button btnNewCount;
        Button btnMainMenu;
        btnNewCount = (Button) findViewById(R.id.btnNewCount);
        btnMainMenu = (Button) findViewById(R.id.btnMainMenu);


        btnNewCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                finish();
                Intent i = new Intent();
                i.setClass(CountEnd.this, Choose.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                finish();
                Intent i = new Intent();
                i.setClass(CountEnd.this, Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }



}

